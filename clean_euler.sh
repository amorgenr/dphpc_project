#!/bin/bash
echo -e "This is a short script to clean up all lsf files.\nBefore you run this script, be aware that the lsf files contain all log outputs.\n"
echo -e "remove all lsf.* files?\ny\tyes,\nn\tno"
read input
if [ $input = "y" ] 
then
  rm -f lsf.*
  echo "deleted all lsf.*"
else
  echo "nothing changed."
fi

