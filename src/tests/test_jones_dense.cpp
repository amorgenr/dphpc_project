//graph
#include<dense_matrix_graph.hpp>

//coloring
#include<jones_seq.hpp>

//other
#include<cassert>
#include<iomanip>

//REMOVE?
void multiTest(size_t n, double p, size_t reps, size_t seed=1998){
  double total_time = 0.;
  for (size_t i = 0; i < reps; i++) {
    Graph_dense_matrix G(n,p);
    assert(G.is_connected() && "not connected");
    Jones_seq<Graph_dense_matrix> J(G);
    J.perform_coloring();
    total_time += J.get_duration();
    J.get_colors();
  }
  std::cout << "testing jones sequential with: "
            << "\n n: "            <<  n
            << "\n p: "            <<  p
            << "\n reps : "        <<  reps
            << "\n average time: " << total_time/reps
            << "\n seed : "        << seed   << std::endl;

}

void oneTest(size_t n, double p){
    if(p < std::log(n)/n){
      std::cout << "warning: graph with n = " << n << ", p = " << p << "may not be connected." << std::endl;
    }
    Graph_dense_matrix LargeGraph(n, p);
    Jones_seq<Graph_dense_matrix> J(LargeGraph);
    J.perform_coloring();
    J.write_to_csv("");
    std::cout << n << std::endl;
}

//Test various n-values for a fixed p-value. Increment is increased every 10 times.
void sizeruntimeTest(const size_t n_min,const size_t n_max,
                      size_t inc,const size_t repetitions,
                      const double p,const std::string filename,
                      const bool add_date){
  assert(n_min <= n_max && "test_jones_dense/sizeruntimeTest: n_min must be smaller than n_max!");

  size_t counter = 0;
  //If n_min and inc are of both powers of the same number, the constant = that number - 1
  int countermax = 9 * n_min / inc;
  std::cout << countermax << std::endl;
  for(size_t n = n_min; n < n_max; n += inc){
    for(size_t i = 0; i < repetitions; i++){
      Graph_dense_matrix LargeGraph(n, p);
      Jones_seq<Graph_dense_matrix> J(LargeGraph);
      J.perform_coloring();

      //Write data to csv
      if(n == n_min && i==0) J.write_to_csv(filename, false, add_date);
      else J.write_to_csv(filename, true, add_date);
      std::cout << n << std::endl;

      if(counter == countermax){
        counter = 1;
        inc *= 10;
      }
    }
    counter++;
  }
}

void LeoTest(size_t n_min, size_t n_max, double p, size_t rep){
  for (size_t i = n_min; i <= n_max; i*=2) {
  double duration = 0.;
  double duration2 = 0.;
    for (size_t j = 0; j < rep; j++) {
      Graph_dense_matrix G(i,p);
      Jones_seq<Graph_dense_matrix> J(G);
      //Jones_seq<Graph_eigen_matrix> E(G);
      J.perform_coloring();
      J.check_correctness();
      //E.perform_coloring();
      //E.check_correctness();
      assert(J.is_correct());
      //assert(E.is_correct());
      duration += J.get_duration();
      //duration2+= E.get_duration();
    }
  std::cout << std::setw(10) << i <<" " << duration/rep/* << " " << duration2/rep */<< std::endl;

  }
}

int main() {
    /*
    for(int i = 2;i < 100000; i*=2){
        double p = std::log(0.2*i)/i;
        oneTest(i,p);
    }
    */
    //sizeruntimeTest(100, 1000, 100, 100, 0.01, "size_runtime_2", false);
    //sizeruntimeTest(100,10000,1,1,0.8,"Leo_Test_03_stdbool",false);
    #ifdef NOBFS
    std::cout << "warning: no connectivity check for graph!\n";
    #endif

    //LeoTest(16, 1024, 0.8, 1000);
    //create plots for report
    std::string csvname = "Jones_Plassman_Runtime_large_n";
    sizeruntimeTest(1000, 50000, 100, 10, 0.01, csvname, false);
    return 0;
}
