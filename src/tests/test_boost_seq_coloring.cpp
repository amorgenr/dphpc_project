#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

//https://www.boost.org/doc/libs/1_71_0/libs/graph/doc/sequential_vertex_coloring.html
#include <boost/graph/sequential_vertex_coloring.hpp>

//https://www.boost.org/doc/libs/1_62_0/libs/graph/doc/erdos_renyi_generator.html
#include <boost/graph/erdos_renyi_generator.hpp>
#include <boost/random/linear_congruential.hpp>

#include <boost/graph/graphviz.hpp>

#include <iostream>

using namespace boost;

int main(){
  typedef adjacency_list<> Graph;
  typedef graph_traits<Graph>::vertices_size_type vertices_size_type;
  typedef property_map<Graph, vertex_index_t>::const_type vertex_index_map;
  typedef boost::erdos_renyi_iterator<boost::minstd_rand, Graph> ERGen;
  minstd_rand gen;

  int n = 10;
  double p = 1.0;
  //creates the same edge multiple times sometimes???
  Graph g(ERGen(gen, n, p, false), ERGen(), n);
  write_graphviz(std::cout, g);
  std::vector<vertices_size_type> color_vec(num_vertices(g));
  iterator_property_map<vertices_size_type*, vertex_index_map> color(&color_vec.front(), get(vertex_index, g));
  vertices_size_type num_colors = sequential_vertex_coloring(g, color);
  std::cout << num_colors << '\n';
  return 0;
}
