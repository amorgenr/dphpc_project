#include "dense_matrix_graph.hpp"

int main() {
  //test 1: check connectness of the graph with x repetitions
  size_t repetitions;
  size_t n;
  double p;
  std::cin >> repetitions >> n >> p;
  size_t counter = 0;

  

  for(size_t i = 0; i<repetitions; i++){
    Graph_dense_matrix temp(n,p);
    if(temp.is_connected()) counter++;
    std::cout << i << std::endl;

  }
  std::cout << "rep:     " <<  repetitions << std::endl
            << "n:       " <<  n           << std::endl
            << "p:       " <<  p           << std::endl
            << "counter: " <<  counter     << std::endl;

  return 0;
}
