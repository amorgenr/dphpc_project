//graph
#include<dense_matrix_graph.hpp>

//coloring
#include<jones_seq.hpp>
#include<MIS_seq.hpp>

int main(){
  size_t n = 1000;
  double p = 0.1;
  Graph_dense_matrix G(n, p);
  if(n<20) std::cout << G << std::endl;

  //TEST 1: coloring for jones and dense matrix
  Jones_seq<Graph_dense_matrix> J(G);
  J.perform_coloring();
  J.check_correctness();
  // if(n<20) J.get_colors();
  std::cout << "coloring for jones algo with a dense matrix correct : " << J.is_correct() << std::endl;

  //TEST 2: coloring for MIS_seq and dense matrix
  MIS_seq<Graph_dense_matrix> J2(G);
  J2.perform_coloring();
  J2.check_correctness();
  //if(n<20) J2.get_colors();
  std::cout << "coloring for MIS algo with a dense matrix correct : " << J2.is_correct() << std::endl;

  return 0;
}
