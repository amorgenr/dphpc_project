//graph
#include<adlist_graph.hpp>

//coloring
//#include<jones_seq_adlist.hpp>
#include<jones_seq.hpp>

int main(){
  auto G = Graph_adlist(100, 0.1, 1337);
  std::cout << G << std::endl;
  auto C = Jones_seq<Graph_adlist>(G);
  C.perform_coloring();
  C.check_correctness();
  C.get_colors();
  std::cout << C.is_correct() << std::endl;
  return 0;
}
