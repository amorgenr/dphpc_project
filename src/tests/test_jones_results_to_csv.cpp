//graph
#include<dense_matrix_graph.hpp>

//coloring
#include<jones_seq.hpp>

int main(){
  std::string filename = "";
  Graph_dense_matrix test(10,1);
  std::cout << test << std::endl;
  Jones_seq<Graph_dense_matrix> testcoloring(test);
  testcoloring.perform_coloring();
  testcoloring.get_colors();
  testcoloring.write_to_csv(filename);
}
