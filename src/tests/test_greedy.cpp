#include "dense_matrix_graph.hpp"
#include "greedy_seq.hpp"


int main(int argc, char const *argv[]) {
  Graph_dense_matrix G(4, 0.5);
  Greedy<Graph_dense_matrix> C(G);
  C.perform_coloring();
  C.check_correctness(false);
  C.get_colors();
  std::cout << C.is_correct() << std::endl;
  std::cout << C.get_duration() << std::endl;
  return 0;
}
