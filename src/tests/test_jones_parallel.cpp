//graph
#include<dense_matrix_graph.hpp>

//coloring
#include "jones_par.hpp"
#include "jones_seq.hpp"

//other
#include<cassert>

typedef Graph_adlist G_type;

int main(int argc, char const *argv[]) {

  std::vector<size_t> n_ = {10, 100, 1000, 2000};
  std::vector<double> p;
  for (size_t i = 1; i < 11; i++) {
    p.push_back(i*0.1);
  }
  size_t reps = 1;
  for (size_t i = 0; i < p.size(); i++) {
    for (size_t j = 0; j < n_.size(); j++) {
      std::cout << "n: " << n_[j] << ", p: " << p[i] << std::endl;
      for (size_t k = 0; k < reps; k++) {

        Graph_adlist Ga(n_[j], p[i], reps);
        Graph_dense_matrix Gd(n_[j], p[i], reps);
        Jones_par<Graph_adlist> J(Ga);
        Jones_par<Graph_dense_matrix> J2(Gd);

        Jones_seq<Graph_adlist> J3(Ga);
        Jones_seq<Graph_dense_matrix> J4(Gd);

        J.perform_coloring();
        J.check_correctness();
        std::cout << J.get_duration() << std::endl;
        std::cout << J.is_correct() << std::endl;
        J2.perform_coloring();
        J2.check_correctness();
        std::cout << J2.get_duration() << std::endl;
        std::cout << J2.is_correct() << std::endl;
        J3.perform_coloring();
        J3.check_correctness();
        std::cout << J3.get_duration() << std::endl;
        std::cout << J3.is_correct() << std::endl;
        J4.perform_coloring();
        J4.check_correctness();
        std::cout << J4.get_duration() << std::endl;
        std::cout << J4.is_correct() << std::endl;

      }
    }
  }

  return 0;
}
