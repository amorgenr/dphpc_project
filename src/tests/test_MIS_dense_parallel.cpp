//graph
#include<dense_matrix_graph.hpp>

//coloring
#include<MIS_par.hpp>
#include<MIS_seq.hpp>

//other
#include<cassert>

//Test various n-values for a fixed p-value. Increment is increased every 10 times.
void sizeruntimeTest(const size_t n_min,const size_t n_max,
  size_t inc,const size_t repetitions,
  const double p,const std::string filename,
  const bool add_date){
    assert(n_min <= n_max && "test_MIS_dense/sizeruntimeTest: n_min must be smaller than n_max!");

    size_t counter = 0;
    //If n_min and inc are of both powers of the same number, the constant = that number - 1
    int countermax = 9 * n_min / inc;

    for(size_t n = n_min; n < n_max; n += inc){
      for(size_t i = 0; i < repetitions; i++){
        Graph_dense_matrix LargeGraph(n, p);
        MIS_par<Graph_dense_matrix> J(LargeGraph);
        J.perform_coloring();

        //Write data to csv
        if(n == n_min) J.write_to_csv(filename, false, add_date);
        else J.write_to_csv(filename, true, add_date);
        std::cout << n << std::endl;

        // if(counter == countermax){
        //   counter = 1;
        //   inc *= 10;
        // }
        counter++;
      }
    }
  }

  void compareTest(){
      for(size_t n = 100; n < 1000; n += 100){
        for(size_t i = 0; i < 2; i++){
          Graph_dense_matrix LargeGraph(n, 0.1);
          MIS_seq<Graph_dense_matrix> M(LargeGraph);
          MIS_par<Graph_dense_matrix> J(LargeGraph);
          std::cout << "sequential: " << '\n';
          M.perform_coloring();
          std::cout << "check correctness: ";
          M.check_correctness();
          std::cout << M.is_correct() << '\n';
          std::cout << "parallel: " << '\n';
          J.perform_coloring();
          std::cout << "check correctness: ";
          J.check_correctness();
          std::cout << J.is_correct() << '\n';
          std::cout << '\n';
          std::cout << n << std::endl;

          // if(counter == countermax){
          //   counter = 1;
          //   inc *= 10;
          //
        }
      }
    }

  void oneTest(size_t n, double p){
    if(p < std::log(n)/n){
      std::cout << "warning: graph with n = " << n << ", p = " << p << "may not be connected." << std::endl;
    }
    Graph_dense_matrix LargeGraph(n, p);
    MIS_par<Graph_dense_matrix> J(LargeGraph);
    J.perform_coloring();
    J.write_to_csv("");
    std::cout << n << std::endl;
  }

  int main() {
    size_t n = 100;
    double p = 0.3;
    size_t count = 1;
    double total_time = 0.;
    // for (size_t i = 0; i < count; i++) {
    //   Graph_dense_matrix G(n,p);
    //   assert(G.is_connected() && "not connected");
    //   MIS_seq<Graph_dense_matrix> C(G);
    //   C.perform_coloring();
    //   total_time += C.get_duration();
    // }
    //sizeruntimeTest(1000,10000,1000,5,0.1,"MIS_runtime_test",false);
    compareTest();
    //~ oneTest(10000,0.1);
    // std::cout << total_time/count << std::endl;

    return 0;
  }
