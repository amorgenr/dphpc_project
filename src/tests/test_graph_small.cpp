//graph
#include "dense_matrix_graph.hpp"
#include "sparse_matrix_graph.hpp"
#include "adlist_graph.hpp"

int main(){

  //should work properly, edges should be ordered

  //assert testing
  //out of bounds
  //g(10,2);

  //circular
  //g(0,0);
  //already exists
  //g(4,2);

  //test 2: graph dense matrix
  std::cout << "test 2: graph dense matrix: " << std::endl;
  Graph_dense_matrix alpha(10,.3);
  std::cout << alpha << std::endl;


  //test 3: graph sparse matrix
  std::cout << "test 3: graph sparse matrix" << std::endl;
  Graph_sparse_matrix beta(10,.3);
  std::cout << beta << std::endl;

  //test 4: adlist and greedy algo draft
  std::cout << "test 4: adlist" << std::endl;
  const size_t n = 10;
  Graph_adlist G_adlist(n);
  for (size_t i = 0; i < n; i++) {
    if(i==n-1) break;
    G_adlist(i,i+1);
  }
  std::cout << G_adlist << std::endl;
  return 0;
}
