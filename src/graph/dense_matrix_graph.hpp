/*Dense Matrix Class, using Ajacency Matrix -- Derived from Base Class*/
#ifndef DENSE_MATRIX_GRAPH_HPP
#define DENSE_MATRIX_GRAPH_HPP

//Graph Base Class
#include "graph.hpp"

//other
#include <random>
#include <queue>
#include <vector>
#include <iostream>

//graph class with adjency matrix, based on std::bool
class Graph_dense_matrix: public Graph {
private:
  //bool array to represent the matrix
  std::vector<bool> adjacency_;
  //helper functions
  void bfs();
  void addEdges();
public:
  //constructor
  Graph_dense_matrix (const size_t, const double, size_t seed=1337);

  //copy constructor
  Graph_dense_matrix(const Graph_dense_matrix&);

  //printer
  friend std::ostream& operator<<(std::ostream&, const Graph_dense_matrix&);

  //returns if the graph is connected
  inline bool is_connected() const;

  inline bool operator() (const size_t,const size_t) const;
  //bool& operator()( size_t&, size_t&);
  void add_edge(size_t, size_t);

  inline int degree(size_t);

  inline int diagonal_sum();

  inline void delete_node(size_t);

  inline bool exist(size_t);
};

//copy constructor
Graph_dense_matrix::Graph_dense_matrix(const Graph_dense_matrix &G1): Graph(G1){
  adjacency_ = G1.adjacency_;
}






bool Graph_dense_matrix::operator()(const size_t i, const size_t j) const {
  assert(i<size_&&j<size_&&"indices out of bounds!");
  return adjacency_[((i<j) ? i*size_+j : j*size_+i)];
}



void Graph_dense_matrix::add_edge(size_t i, size_t j){
  assert(i<size_&&j<size_&&"indices out of bounds!");
  adjacency_[((i<j) ? i*size_+j : j*size_+i)]=true;
}

//default constructor for Adjacency-Matrix nxn
Graph_dense_matrix::Graph_dense_matrix(const size_t n, const double prob, size_t seed):
                                                               Graph(n,prob,seed),
                                                               adjacency_(std::vector<bool>(n*n,false))
{
  addEdges();
}

//create edges
void Graph_dense_matrix::addEdges() {
  do{
    //adjacency_.fill(false);
    std::fill(adjacency_.begin(),adjacency_.end(),false);
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    for (int i = 0; i < size_; i++) {
      adjacency_[(i*size_+i)] = true;
      for (int j = i+1; j < size_; j++) {
        double r = dis(generator_);
        if (r <= p_) {
          adjacency_[((i<j) ? i*size_+j : j*size_+i)] = true;
          //adjacency_(j,i) = true;
        }
      }
    }

  #ifdef NOBFS
  }
  while (false);
  #else
  bfs();
  }
  while(!is_connected());
  #endif
}

//returns the connectedness of the graph
bool Graph_dense_matrix::is_connected() const {
  return is_connected_;
}

//std printer
std::ostream& operator<<(std::ostream& os, const Graph_dense_matrix& g)
{
  os << "Adjacency Matrix\n";
  for (size_t i = 0; i < g.size_; i++) {
    for (size_t j = 0; j < g.size_; j++) {
      //os << g.adjacency_[(i>j?i:j)][(i>j?i:j)];
      os << g.adjacency_[(i<j)?i*g.size_+j:j*g.size_+i];
    }
    os << "\n";
  }
  return os;
}

//perform bfs on graph
void Graph_dense_matrix::bfs(){
  //create visited array
  std::vector<bool> visited(size_, 0);
  //visited.fill(false);

  //create q and push node 0
  visited[0] = true;
  std::queue<size_t> q;
  q.push(0);

  while(!q.empty()){
    size_t temp = q.front();
    q.pop();
    for(size_t i = 0; i < size_ ; i++){
      if(!visited[i] && adjacency_[(i<temp)?i*size_+temp:temp*size_+i]) {
        visited[i] = true;
        q.push(i);
      }
    }
  }
  //check if all nodes have been visited, if one hasn't been, return
  for (size_t i = 0; i < size_; i++) {
    if(!visited[i]) return;
  }
  //flip the bool and return
  is_connected_ = true;
  return;
}

//returns degree of node
int Graph_dense_matrix::degree(size_t i) {
  int temp = 0;
    for (size_t j = 0; j < size_; j++) {
      if (i==j) continue;
      if(adjacency_[((i<j) ? i*size_+j : j*size_+i)])
        ++temp;
    }
    return temp;
}

//returns sum over diagonal
int Graph_dense_matrix::diagonal_sum() {
  int temp = 0;
  for (size_t i = 0; i < size_; i++) {
    if (adjacency_[(i*size_+i)]) ++temp;
  }
  return temp;
}

void Graph_dense_matrix::delete_node(size_t i) {
  adjacency_[(i*size_+i)]=false;
}

bool Graph_dense_matrix::exist(size_t i) {
  return adjacency_[(i*size_+i)];
}

#endif
