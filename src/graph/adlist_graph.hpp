#ifndef ADLIST_GRAPH_HPP
#define ADLIST_GRAPH_HPP

#include "graph.hpp"

#include <set>
#include <iterator>
#include <algorithm>
#include<vector>
#include<queue>

class Graph_adlist: public Graph {
private:
  std::vector<std::set<size_t>> adj_;
  std::vector<bool> node_exist_;
  bool is_connected_;
  void add_edges();
  void bfs();

public:
  Graph_adlist (const size_t, const double, size_t seed = 1337);
  //inline bool edge_exists(const size_t, const size_t) const;
  inline bool operator()(const size_t,const size_t);
  friend std::ostream& operator<<(std::ostream&, const Graph_adlist&);
  inline std::set<size_t>& get_list(size_t);
  inline void delete_edge(size_t, size_t);
  inline void delete_node(size_t);
  inline int diagonal_sum();
  inline bool exist(size_t);
};

void Graph_adlist::add_edges(){
  do{
    adj_.clear();
    adj_.resize(size_);
    node_exist_.resize(size_);
    std::uniform_real_distribution<double> dis(0.0, 1.0);
    for(int i = 0; i < size_; i++){
      node_exist_[i] = true;
      for(int j = i+1; j < size_; j++){
        double r = dis(generator_);
        if (r <= p_) {
          adj_[i].insert(j);
          adj_[j].insert(i);
        }
      }
    }
    bfs();
  }
  while(!is_connected_);
}

//returns if there exists an edge.
bool Graph_adlist::operator()(const size_t i,const size_t j){
  //assert(i<size_&&j<size_&&"indices out of bounds!");
  return adj_[i].find(j)!=adj_[i].end();
}

std::ostream& operator<<(std::ostream& os, const Graph_adlist& G){
  for (size_t i = 0; i < G.size_; ++i) {
    std::cout << i << " : ";
    for (auto j : G.adj_[i]) {
      std::cout << j << " ";
      }
      std::cout << std::endl;
    }
  return os;
}

void Graph_adlist::bfs(){
  is_connected_ = false;
  std::vector<bool> visited(size_, 0);
  visited[0] = true;
  std::queue<size_t> q;
  q.push(0);
  while(!q.empty()){
    size_t temp = q.front();
    q.pop();
    for (auto i = adj_[temp].begin(); i != adj_[temp].end(); i++) {
      if(!visited[*i]){
        visited[*i]=true;
        q.push(*i);
      }
    }
  }
  auto finder = std::find(visited.begin(), visited.end(), false);
  is_connected_ = (finder == visited.end());
}

Graph_adlist::Graph_adlist(const size_t n, const double p, const size_t seed): Graph{n, p, seed}, is_connected_(false){
  add_edges();
}

std::set<size_t>& Graph_adlist::get_list(size_t i){
  return adj_[i];
}

void Graph_adlist::delete_edge(size_t i, size_t j) {
  adj_[i].erase(j);
  adj_[j].erase(i);
}

void Graph_adlist::delete_node(size_t i){
  //if (node_exist_[i]) {
    for (auto k: adj_[i]) {
      adj_[i].erase(k);
      adj_[k].erase(i);
    }
    node_exist_[i] = false;
  //}
}

int Graph_adlist::diagonal_sum() {
  int temp = 0;
  for (size_t i = 0; i < size_; i++) {
    if (node_exist_[i]) ++temp;
  }
  return temp;
}

bool Graph_adlist::exist(size_t i) {
  return node_exist_[i];
}

#endif
