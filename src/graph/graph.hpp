/* basic graph class */
#ifndef GRAPH_HPP
#define GRAPH_HPP

//other
#include<vector>
#include<cassert>
#include<iostream>
#include<random>
//abstract base class for the graph
class Graph {
protected:
  //maximal size of the graph
  size_t size_;
  //true if the graph is connected
  bool is_connected_;
  //seed of graph
  size_t seed_;
  //engine
  std::mt19937 generator_;
  //probability
  double p_;

public:
  Graph (const size_t, double, size_t seed=1337);

  Graph (const Graph &G1);

  inline double get_probability() const;

  //virtual bool operator()(const size_t,const size_t) const = 0;
  //virtual bool& operator()( size_t&, size_t&);
  virtual ~Graph() = default;
  inline size_t get_size() const;

};

Graph::Graph(const size_t n, double p, size_t seed) : size_(n),
                                                     is_connected_(false),
                                                     seed_{seed},
                                                     p_(p),
                                                     generator_{std::mt19937(seed)}{
                                                       assert(p!=0. && "p shouldn't be 0!");
}

double Graph::get_probability() const {
  return p_;
}


Graph::Graph (const Graph &G1) :
  size_(G1.size_),
  is_connected_(G1.is_connected_),
  seed_{G1.seed_},
  generator_{G1.generator_},
  p_(G1.p_)
  {}

size_t Graph::get_size() const{
  return size_;
}

#endif
