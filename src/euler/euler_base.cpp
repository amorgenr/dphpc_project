//this is the base benchmark for our algorithms
//to use tests - comment in/out tests in CMakeLists.txt

//graph
#include <graph.hpp>
#include <dense_matrix_graph.hpp>
#include <adlist_graph.hpp>

//coloring
#include <coloring.hpp>
#include <greedy_seq.hpp>
#include <jones_seq.hpp>
#include <jones_par.hpp>
#include <MIS_seq.hpp>
#include <MIS_par.hpp>
#include <MIS_par_deg.hpp>

//other
#include <utility> //for pair
#include <cmath> //for computing minimal p

//typedefs
typedef std::pair<size_t,double> test;

//global vars (some lazyness and I don't want to have 8 strings as function parameters)
std::string JP_mat_seq_name = "JP_mat_seq";
std::string MIS_mat_seq_name = "MIS_mat_seq";
std::string JP_mat_par_name = "JP_mat_par";
std::string MIS_mat_par_name = "MIS_mat_par";
std::string JP_adl_seq_name = "JP_adl_seq";
std::string MIS_adl_seq_name = "MIS_adl_seq";
std::string JP_adl_par_name = "JP_adl_par";
std::string MIS_adl_par_name = "MIS_adl_par";

std::string MIS_mat_par_deg_name = "MIS_mat_par_deg";
std::string MIS_adl_par_deg_name = "MIS_adl_par_deg";

//README:
// one_test is a templated test to make the function more readable
// correctness needs a vector of pairs where one pair represents one test case
// set the flags in the CMakeLists.txt to select the tests you want to run


template<class Gr, template<class Gr1>class C>
inline
void one_test(size_t n, double p, double reps, std::string filename, bool append){
  double tempdur = 0;
  double totaldur = 0;
  double maxdur = 0;

  for(int i = 0; i < reps; i++){
    Gr G(n, p, i);
    C<Gr> temp(G);
    temp.perform_coloring();
    #ifdef CORRECTCHECK
    temp.check_correctness();
    #endif
    #ifdef NROFCOLORS
    int nr_colors = temp.get_nr_colors();
    std::cout << "Coloring used "<< nr_colors << " colors.\n";
    #endif
    temp.write_to_csv(filename, append, false);
    tempdur = temp.get_duration();
    totaldur += tempdur;
    maxdur = std::max(maxdur, tempdur);
  }
  #ifdef INFO
  double avgdur = totaldur/reps;
  if(avgdur < 1000000){
    std::cout << "Average Time for coloring:\t" << avgdur << " microseconds" << std::endl;
  }
  else{
    std::cout << "Average Time for coloring:\t" << avgdur / 1000000 << " seconds" << std::endl;
  }
  if(maxdur < 1000000){
    std::cout << "Maximal Time for coloring:\t" << maxdur << " microseconds" << std::endl;
  }
  else{
    std::cout << "Maximal Time for coloring:\t" << maxdur / 1000000 << " seconds" << std::endl;
  }
  std::cout << "Number of repetitions:\t" << reps << std::endl;
  #endif
}

//correctness check | input: tests - vector of (n,p) pairs, reps - number of repetitions per (n,p) pair
void correctness(std::vector<test> tests, const size_t reps) {
  //add sizeruntimeTest functionality
  #ifdef JONES_MAT_SEQ
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting Jones-Plassman Matrix Sequential Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << std::endl;
    #endif
    one_test<Graph_dense_matrix, Jones_seq>(n, p, reps, JP_mat_seq_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout << "\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished Jones-Plassman Matrix Sequential Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef JONES_MAT_PAR
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting Jones-Plassman Matrix Parallel Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    one_test<Graph_dense_matrix, Jones_par>(n, p, reps, JP_mat_par_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout <<"\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished Jones-Plassman Matrix Parallel Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef MIS_MAT_SEQ
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Matrix Sequential Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << std::endl;
    #endif
    one_test<Graph_dense_matrix, MIS_seq>(n, p, reps, MIS_mat_seq_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout << "\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Matrix Sequential Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef MIS_MAT_PAR
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Matrix Parallel Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    one_test<Graph_dense_matrix, MIS_par>(n, p, reps, MIS_mat_par_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout <<"\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Matrix Parallel Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef JONES_ADL_SEQ
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting Jones-Plassman Adlist Sequential Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << std::endl;
    #endif
    one_test<Graph_adlist, Jones_seq>(n, p, reps, JP_adl_seq_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout <<"\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished Jones-Plassman Adlist Sequential Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef JONES_ADL_PAR
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting Jones-Plassman Adlist Parallel Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    one_test<Graph_adlist, Jones_par>(n, p, reps, JP_adl_par_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout << "\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished Jones-Plassman Adlist Parallel Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef MIS_ADL_SEQ
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Adlist Sequential Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = "<< p << std::endl;
    #endif
    one_test<Graph_adlist, MIS_seq>(n, p, reps, MIS_adl_seq_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout <<"\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Adlist Sequential Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef MIS_ADL_PAR
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Adlist Parallel Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = " << p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    one_test<Graph_adlist, MIS_par>(n, p, reps, MIS_adl_par_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout << "\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Adlist Parallel Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  //New degree test

  #ifdef MIS_MAT_PAR_DEG
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Matrix Parallel New Degree Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = " << p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    if(it == *(tests.begin())){
      one_test<Graph_dense_matrix, MIS_par_deg>(n, p, reps, MIS_mat_par_deg_name, true);
    }
    one_test<Graph_dense_matrix, MIS_par_deg>(n, p, reps, MIS_mat_par_deg_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout <<"\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Matrix Parallel New Degree Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif

  #ifdef MIS_ADL_DEG_PAR
  #ifdef INFO
  std::cout << "\n--------------------------------------------------" << std::endl;
  std::cout << "Starting MIS Adlist Parallel New Degree Test:" << std::endl;
  std::cout << "--------------------------------------------------\n" << std::endl;
  #endif
  for (auto const &it: tests){
    size_t n = it.first;
    double p = it.second;
    #ifdef INFO
    std::cout << "starting for: n = " << n  << ", p = " << p << ", #threads = " << std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1) << std::endl;
    #endif
    if(it == *(tests.begin())){
      one_test<Graph_adlist, MIS_par_deg>(n, p, reps, MIS_adl_par_name, true);
    }
    one_test<Graph_adlist, MIS_par_deg>(n, p, reps, MIS_adl_par_name, true);
    #ifdef INFO
    std::cout << "finished for: n = " << n  << ", p = "<< p << std::endl;
    std::cout << "\n";
    #endif
  }
  #ifdef INFO
  std::cout << "--------------------------------------------------" << std::endl;
  std::cout << "Finished MIS Adlist Parallel New Degree Test." << std::endl;
  std::cout << "--------------------------------------------------" << std::endl;
  #endif
  #endif
}

void add_small_tests(std::vector<test>& tests){
  tests.emplace_back(10, 0.4);
  tests.emplace_back(100, 0.05);
  tests.emplace_back(20, 0.2);
  tests.emplace_back(200, 0.05);
}

void add_complete_dense_tests(std::vector<test>& tests){
  tests.emplace_back(1, 1);
  tests.emplace_back(2, 1);
  tests.emplace_back(5, 1);
  tests.emplace_back(10, 0.4);
  tests.emplace_back(100, 0.05);
  tests.emplace_back(1000, 0.01);
  //tests.emplace_back(10000, 1);
}

void add_big_tests(std::vector<test>& tests){
  for(int n = 1000; n <= 10000; n += 100){
    tests.emplace_back(n, 0.01);
  }
}

void add_varying_p(std::vector<test>& tests, double max_p, double n, double p_step){
  double min_p = std::log(n)/n;
  double safe_p = 1.01 * min_p;
  for(double p = safe_p; p  < max_p; p += p_step){
    tests.emplace_back(n, p);
  }
}

void minimal_p(std::vector<test>& tests, int min_n, int max_n, int n_step){
  for(double n = min_n; n <= max_n; n += n_step){
    double min_p = std::log(n)/n;
    double safe_p = 1.01 * min_p;
    tests.emplace_back(n, safe_p);
  }
}

int main() {
  //exit if no tests are selected
  #ifndef JONES_MAT_SEQ
  #ifndef MIS_MAT_SEQ
  #ifndef JONES_MAT_PAR
  #ifndef MIS_MAT_PAR
  #ifndef JONES_ADL_SEQ
  #ifndef MIS_ADL_SEQ
  #ifndef JONES_ADL_PAR
  #ifndef MIS_ADL_PAR

  std::cout << "ERROR: no test selected! Please add compile flags to start tests!\n";
  return 0;

  #endif
  #endif
  #endif
  #endif
  #endif
  #endif
  #endif
  #endif

  std::vector<test> tests_correctness;
  //add_small_tests(tests_correctness);
  //add_complete_dense_tests(tests_correctness);
  //add_big_tests(tests_correctness);
  //add_varying_p(tests_correctness, 0.1, 10000, 0.001);
  //minimal_p(tests_correctness, 1000, 30000, 1000);

  tests_correctness.push_back(std::make_pair(10000, 0.015));

  correctness(tests_correctness, 1);

  return 0;
}
