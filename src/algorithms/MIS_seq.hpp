//coloring
#include "coloring.hpp"

//graph
#include "dense_matrix_graph.hpp"
#include "adlist_graph.hpp"

//other
#include <random>
#include <list>
#include <iterator>

/*
removed because included in coloring.hpp
#include <vector>
*/
template <class Gr>
class MIS_seq: public Coloring<Gr> {
private:
  std::vector<size_t> degrees_;
  std::vector<size_t> degrees_begin_;
  std::list<size_t> S_;
  std::list<size_t> I_;
  Gr V_;
  std::list<size_t> uncolored_;
public:
  MIS_seq (const Gr&);
  void perform_coloring();
};

template<class Gr>
MIS_seq<Gr>::MIS_seq(const Gr& G): Coloring<Gr> (G), V_(G){

}

template<class Gr>
void MIS_seq<Gr>::perform_coloring(){
  //std::cout << "starting coloring\n";
  //std::cout << "finished coloring\n";
}

//Coloring Adlist
template<>
void MIS_seq<Graph_adlist>::perform_coloring(){
  //START TIMING HERE
  t1_ = std::chrono::high_resolution_clock::now();

  // Setup
  // Different RNG
  std::mt19937 generator (1337);
  std::uniform_real_distribution<double> dis(0.0, 1.0);

  int n = G_.get_size();
  int current_color = 0;
  I_.resize(0);
  bool unfinished = true;
  int count_while = 1;

  //START OF COLORING
  for(int g_size = G_.get_size(); g_size > 0; --g_size){

    ++current_color;

    V_ = G_;

    //std::cout << "SIZE of G: " << G_.diagonal_sum() << '\n';
    //While V isnt empty set => Determine a MIS
    while(V_.diagonal_sum() != 0){
      //std::cout << "Size of V: " << V_.diagonal_sum() << '\n';
      // Clear Temps
      std::vector<size_t> S_;

      // Determine degrees of nodes
      for (size_t i = 0; i < n; i++) {
        if (!(V_.exist(i))) continue;

        // Determine random set of vertices S_ in V_
        double r = dis(generator);
        int deg = V_.get_list(i).size();
        if (deg == 0) {
            S_.push_back(i);
          continue;
        }
        double temp = 1.0/(2.0 * (deg));
        if(r < temp) {
            S_.push_back(i);
        }
      }

      //std::cout << "Size of S: " << S_.size() << '\n';

      for(auto i = S_.begin(); i != S_.end(); ++i){
        for(auto j = S_.begin(); j != S_.end(); ++j){
          if (i == j) continue;
          if(V_(*i,*j)){
            if(V_.get_list(*i).size() < V_.get_list(*j).size()){
              i=std::prev(i,1);
              S_.erase(std::next(i,1));
              break;
            }
            else{
              j=std::prev(j,1);
              S_.erase(std::next(j,1));
            }
          }
        }
      }
      //std::cout << "SIze of S': " << S_.size() << '\n';

      //Add set S_ to I_
      for(int x = 0; x < S_.size(); ++x){
        I_.push_back(S_[x]);
      }

      //Remove from V the set S and all the neighbours of nodes in S.
      for (auto s_vertex : S_) {
        for (auto j: V_.get_list(s_vertex)){
          if (!(V_.exist(j))) continue;
          //delete neighbours
          V_.delete_node(j);
        }
        V_.delete_node(s_vertex);
      }
    }

    for (auto i: I_) {
        if (!(G_.exist(i))) continue;
          G_.delete_node(i);
          --g_size;
        colors_[i] = current_color;
    }
    g_size++;
    if (G_.diagonal_sum() == 0) g_size = 0;

    //empty the MIS for the next round
    I_.clear();
  }

  t2_ = std::chrono::high_resolution_clock::now();
  success_ = 1;
  duration_ = std::chrono::duration<double, std::micro>(t2_- t1_);
  std::cout << "finished in " << duration_.count() << " microseconds!\n";
  nr_colors_used_ = current_color;
}

//Coloring Dense Matrix
template<>
void MIS_seq<Graph_dense_matrix>::perform_coloring(){
  //START TIMING HERE
  t1_ = std::chrono::high_resolution_clock::now();

  // Setup
  // Different RNG
  std::mt19937 generator (1337);
  std::uniform_real_distribution<double> dis(0.0, 1.0);

  //TIMING SETUP
  std::chrono::duration<double, std::micro> duration1;
  std::chrono::duration<double, std::micro> duration2;
  std::chrono::duration<double, std::micro> duration3;

  int n = G_.get_size();
  int current_color = 0;
  degrees_.resize(n);
  degrees_begin_.resize(n);
  I_.resize(0);
  bool unfinished = true;
  int count_while = 1;

  uncolored_.resize(n);
  std::iota(uncolored_.begin(), uncolored_.end(), 0);

  //START OF COLORING

  // Determine degrees of nodes
  for (size_t i = 0; i < n; i++) {
    degrees_begin_[i] = G_.degree(i);
  }

  while(G_.diagonal_sum() != 0) {
    ++current_color;
    V_ = G_;

    degrees_ = degrees_begin_;
    //While V isnt empty set => Determine a MIS
    while(V_.diagonal_sum() != 0){
      // Clear Temps
      S_.clear();

      /*// Determine degrees of nodes
      for (size_t i = 0; i < n; i++) {
        if (V_(i,i) == 0) continue;
        int temp = -1; //-1 because of i,i == 1 and we have to ignore that in degree
        for (size_t j = 0; j < n; j++) {
          if(V_(i,j))
          ++temp;
        }
        degrees_[i] = temp;
      }*/

      //0.1% of time
      std::chrono::high_resolution_clock::time_point t3 = std::chrono::high_resolution_clock::now();
      // Determine random set of vertices S_ in V_
      for (int i = 0; i < n; ++i) {
        double r = dis(generator);
        if (!(V_(i,i))) continue;
        if (degrees_[i] == 0) {
          S_.push_back(i);
          continue;
        }
        double temp = 1.0/(2.0 * (degrees_[i]));
        if(r < temp) {
          S_.push_back(i);
        }
      }
      std::chrono::high_resolution_clock::time_point t4 = std::chrono::high_resolution_clock::now();
      duration2 = std::chrono::duration<double, std::micro>(t4 - t3);

      //Fast AF
      std::chrono::high_resolution_clock::time_point t5 = std::chrono::high_resolution_clock::now();
      for(auto i = S_.begin(); i != S_.end(); ++i){
        for(auto j = S_.begin(); j != S_.end(); ++j){
          if (i == j) continue;
          if(V_(*i,*j)){
            if(degrees_[*i] < degrees_[*j]){
              i=std::prev(i,1);
              S_.remove(*std::next(i,1));
              break;
            }
            else{
              j=std::prev(j,1);
              S_.remove(*std::next(j,1));
            }
          }
        }
      }
      std::chrono::high_resolution_clock::time_point t6 = std::chrono::high_resolution_clock::now();
      duration3 = std::chrono::duration<double, std::micro>(t6 - t5);

      //Add set S_ to I_
      for(auto x: S_) I_.push_back(x);

      //Remove from V the set S and all the neighbours of nodes in S.
      for (auto s_vertex : S_) {
        for (size_t j = 0; j < n; j++) {
          if (!(V_(j,j))) continue;
          if(V_(s_vertex, j)){
            //lower degree of connected vertices
            for (size_t v = 0; v < n; v++) {
              if ((V_(j,v)))
                --degrees_[v];
            }
            V_.delete_node(j);
          }
        }
        //lower degree of connected vertices
        for (size_t v = 0; v < n; v++) {
          if ((V_(s_vertex,v)))
            --degrees_[v];
        }
        V_.delete_node(s_vertex);
      }
    }
    //Found MIS in I

    t2_ = std::chrono::high_resolution_clock::now();

    //coloring Set I in current color
    for (auto i: I_) {
      //lower degree of connected vertices
      for (size_t v = 0; v < n; v++) {
        if ((V_(i,v)))
          --degrees_begin_[v];
      }
      G_.delete_node(i);
      colors_[i] = current_color;
    }
    //empty the MIS for the next round
    I_.clear();
  }

  t2_ = std::chrono::high_resolution_clock::now();
  success_ = 1;
  duration_ = std::chrono::duration<double, std::micro>(t2_- t1_);
  nr_colors_used_ = current_color;
  //std::cout << "first for loop: " << duration1.count() << " microseconds" << std::endl;
  //std::cout << "second for loop: " << duration2.count() << " microseconds" << std::endl;
  //std::cout << "third for loop: " << duration3.count() << " microseconds" << std::endl;
  //std::cout << "finished in " << duration_.count() << " microseconds!\n";
}
