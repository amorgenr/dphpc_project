#ifndef JONES_SEQ_HPP
#define JONES_SEQ_HPP

//coloring
#include "coloring.hpp"

//graph
#include "dense_matrix_graph.hpp"
#include "eigen_matrix_graph.hpp"
#include "adlist_graph.hpp"

//other
#include <algorithm>

//general class
template<class Gr>
class Jones_seq: public Coloring<Gr> {
//changed to protected to allow inheritance
protected:
  inline bool has_prio(const size_t);
  //std::vector<bool> colors_avail_;
public:
  Jones_seq(const Gr&);
  void perform_coloring();
};

//general constructor
template<class Gr>
Jones_seq<Gr>::Jones_seq(const Gr& G): Coloring<Gr> (G){
}

//general has_prio
template<class Gr>
bool Jones_seq<Gr>::has_prio(const size_t i){return false;}

//NEW: general best color
//template<class Gr>
//int Jones_seq<Gr>::best_color(const size_t i){return -1;}

//general coloring
template<class Gr>
void Jones_seq<Gr>::perform_coloring(){
    std::cout << "starting coloring\n";
    std::cout << "finished coloring\n";
}

//returns if the passed node has prio
//for vertex i check if edge exists from i to all j > i, if edge exists and j is not colored yet
//maybe return bool array of colors of all j?
template<>
bool Jones_seq<Graph_dense_matrix>::has_prio(const size_t i){
    for (size_t j = i+1; j < n_; j++) {
        if(G_(i,j) && !colors_[j]) return false;
    }
    return true;
}

template<>
bool Jones_seq<Graph_adlist>::has_prio(const size_t i){
    for (auto j: G_.get_list(i)) {
        if(j < i) continue;
        if(!colors_[j]) return false;
    }
    return true;
}

template<>
void Jones_seq<Graph_dense_matrix>::perform_coloring(){
    //TIMING START
    t1_ = std::chrono::high_resolution_clock::now();

    //vector as queue
    std::vector<size_t> Q;

    //vector to find available colors
    std::vector<bool> colors_avail_;

    //current color number, set to 1 to color the last node directly.
    size_t current_color = 1;
    //color the last node to avoid if statement later
    colors_.back() = 1;
    while(!success_){
        //find relevant nodes - p = 0.002 30'000 of 67'000, p = 0.2 104'000 of 196'000
        for (size_t i = 0; i < n_; i++) {
            if(colors_[i]) continue;
            if(has_prio(i)) Q.push_back(i);
        }

        //return if we are finished
        if(Q.empty()) success_ = true;
        else{
            size_t min_col = 0;

            //Fast - only ~40 of 200'000
            colors_avail_.resize(current_color + 1);

            //fill with true values - slower for denser matrices but still 130 of 200'000
            std::fill(colors_avail_.begin(), colors_avail_.end(), true);

            //Time spent in this nested for-loop around 50-70% of total runtime
            //Q.size() starts at roughly 1/p then goes to 0.4-0.5 * 1/p
            //std::cout << Q.size() << std::endl;
            for (size_t i = 0; i < Q.size(); i++) {
              size_t temp = Q[i];
              //changed i to temp! -> reduces time spent drastically
              for (size_t j = temp + 1; j < n_; j++) {
                //if there exists an edge between i and j make the color of j unavailable
                if(G_(temp,j)) colors_avail_[colors_[j]] = false;
              }
            }

            //find the minimum color
            //Time spent in this loop very small ~10 microseconds if total runtime is 70'000'000
            for (size_t i = 1; i < colors_avail_.size(); i++) {
                if (colors_avail_[i]) {
                    min_col = i;
                    break;
                }
            }

            //if there is no already used color, define a new color
            if(min_col == 0){
                min_col = ++current_color;
            }

            //fill all relevant colors_
            //Time spent is small ~40 microseconds if total runtime is 200'000 adn even less fore sparse graphs
            for (size_t i = 0; i < Q.size(); i++) {
                colors_[Q[i]] = min_col;
            }

            //Fast - only 25 microseconds of 200'000 if dense, if sparse 0.6 of 70'000
            Q.clear();
        }
    }

    t2_ = std::chrono::high_resolution_clock::now();
    success_ = true;
    duration_ = std::chrono::duration<double, std::micro>(t2_ - t1_);
    nr_colors_used_ = current_color;
}

template<>
void Jones_seq<Graph_adlist>::perform_coloring(){
    t1_ = std::chrono::high_resolution_clock::now();
    //vector as queue
    std::vector<size_t> Q;
    std::chrono::duration<double, std::micro> duration;
    //vector to find available colors
    std::vector<bool> colors_avail_;

    //current color number, set to 1 to color the last node directly.
    size_t current_color = 1;
    //color the last node to avoid if statement later
    colors_.back() = 1;

    while(!success_){
        //find highest prio nodes
        for (size_t i = 0; i < n_ - 1; i++) {
            if (colors_[i]) continue;
            if (has_prio(i)) Q.push_back(i);
        }

        if(Q.empty()){
            success_ = true;
            break;
        }

        //find lowest possible color
        size_t min_col = 0;
        colors_avail_.resize(current_color + 1);
        //fill with true values
        std::fill(colors_avail_.begin(), colors_avail_.end(), true);

        auto t1 = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < Q.size(); i++) {
            for (auto j: G_.get_list(Q[i])) {
                colors_avail_[colors_[j]] = false;
            }
        }
        auto t2 = std::chrono::high_resolution_clock::now();
        duration += std::chrono::duration<double, std::micro> (t2 - t1);

        //find the minimum color
        for (size_t i = 1; i < colors_avail_.size(); i++) {
            if (colors_avail_[i]) {
                min_col = i;
                break;
            }
        }

        //if there is no already used color, define a new color
        if(min_col == 0){
            min_col = ++current_color;
        }

        //fill all relevant colors_
        for (size_t i = 0; i < Q.size(); i++) {
            colors_[Q[i]] = min_col;
        }
        Q.clear();
    }
    t2_ = std::chrono::high_resolution_clock::now();
    success_ = 1;
    duration_ = std::chrono::duration<double, std::micro>(t2_ - t1_);
    nr_colors_used_ = current_color;
}
#endif
