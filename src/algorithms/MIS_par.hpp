//coloring
#include "coloring.hpp"
#include <omp.h>

//graph
#include "dense_matrix_graph.hpp"
#include "adlist_graph.hpp"

//other
#include <random>
#include <list>
#include <iterator>

template <class Gr>
class MIS_par: public Coloring<Gr> {
private:
  std::list<size_t> I_;
  Gr V_;
  std::list<size_t> uncolored_;
  std::vector<size_t> degrees_begin_;
public:
  MIS_par (const Gr&);
  void perform_coloring();
};

template<class Gr>
MIS_par<Gr>::MIS_par(const Gr& G): Coloring<Gr> (G), V_(G){

}

template<class Gr>
void MIS_par<Gr>::perform_coloring(){
  //std::cout << "starting coloring\n";
  //std::cout << "finished coloring\n";
}

//Coloring Adlist
template<>
void MIS_par<Graph_adlist>::perform_coloring(){
  //START TIMING HERE
  t1_ = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::micro> parallel_duration;

  // Setup
  // Different RNG
  std::mt19937 generator (1337);
  std::uniform_real_distribution<double> dis(0.0, 1.0);

  int n = G_.get_size();
  int current_color = 0;
  I_.resize(0);
  bool unfinished = true;
  int count_while = 1;
  //START OF COLORING
  for(int g_size = G_.diagonal_sum(); g_size > 0;){

    //std::cout << "WE are at the beginning with: " << g_size << '\n';

    ++current_color;
    //my_col = current_color;

    V_ = G_;
    //std::cout << "Size of G:" << G_.diagonal_sum() << '\n';

    //While V isnt empty set => Determine a MIS
    //auto misbegin = std::chrono::high_resolution_clock::now();
    auto t1 = std::chrono::high_resolution_clock::now();
    #pragma omp parallel for firstprivate(V_)
    for(int v_size = V_.diagonal_sum(); v_size > 0; --v_size){
      //#pragma omp critical
      //std::cout << "Size of V:" << V_.diagonal_sum() << " with g_size: " << g_size << '\n';
      // Clear Temps
      std::vector<size_t> S_;

      // Determine degrees of nodes
      //#pragma omp parallel for
      //auto degreebegin = std::chrono::high_resolution_clock::now();
      for (size_t i = 0; i < n; i++) {
        if (!(V_.exist(i))) continue;
        // Determine random set of vertices S_ in V_
        double r = dis(generator);
        int deg = V_.get_list(i).size();
        if (deg == 0) {
          S_.push_back(i);
          continue;
        }
        double temp = 1.0/(2.0 * (double)deg);
        if(r < temp) {
          S_.push_back(i);
        }
      }

      /*#pragma omp critical
      {
      std::cout << "Size of V:" << V_.diagonal_sum() << " with g_size: " << g_size << " and S: " << S_.size() << '\n';
      for (auto i: S_) std::cout << i << " ";
      std::cout << '\n';
    }*/

      //auto erasebegin = std::chrono::high_resolution_clock::now();
      for(auto i = S_.begin(); i != S_.end(); ++i){
        /*#pragma omp critical
        {
          //std::cout << "edges of " << *i << ":" << '\n';
          if (V_.get_list(*i).size() == 0){
            std::cout << "0" << '\n';
          }
          else {
          for (auto const& k : V_.get_list(*i))
          {
            std::cout << k << ' ';
          }
        }
          std::cout << '\n';
          //for (auto k: V_.get_list(*i)) std::cout << k << " ";
        } */
        for(auto j = S_.begin(); j != S_.end(); ++j){
          if (i == j) continue;
          //#pragma omp critical
          if(V_(*i,*j)){
            //#pragma omp critical
            //std::cout << "we delete an edge: " << *i <<  " & " << *j << '\n';
            if(V_.get_list(*i).size() < V_.get_list(*j).size()){
              //#pragma omp critical
              //std::cout << "there is an edge between " << *i << *j << '\n';
              i=std::prev(i,1);
              S_.erase(std::next(i,1));
              break;
            }
            else{
              j=std::prev(j,1);
              S_.erase(std::next(j,1));
            }
          }
        }
      }
      //auto eraseend = std::chrono::high_resolution_clock::now();
      //erasedur += std::chrono::duration<double, std::micro>(eraseend - erasebegin).count();

    /*  #pragma omp critical
      {
      std::cout << "Size of V:" << V_.diagonal_sum() << " with g_size: " << g_size << " and S': " << S_.size() << '\n';
      for (auto i: S_) std::cout << i << " ";
      std::cout << '\n';
    }*/

      //Add set S_ to I_
      //auto copySbegin = std::chrono::high_resolution_clock::now();
      for(int x = 0; x < S_.size(); ++x){
        //#pragma omp critical
        I_.push_back(S_[x]);
      }
      //auto copySend = std::chrono::high_resolution_clock::now();
      //copySdur += std::chrono::duration<double, std::micro>(copySend - copySbegin).count();

      //Remove from V the set S and all the neighbours of nodes in S.
      //#pragma omp parallel for
      //auto updateVbegin = std::chrono::high_resolution_clock::now();
      for (auto s_vertex : S_) {
        for (auto j: V_.get_list(s_vertex)){
          if (!(V_.exist(j))) continue;
          //delete neighbours
          #pragma omp critical
          {
          V_.delete_node(j);
          --v_size;
          }
        }
        #pragma omp critical
        {
        V_.delete_node(s_vertex);
        --v_size;
        }
      }
      ++v_size;
      //auto updateVend = std::chrono::high_resolution_clock::now();
      //updateVdur += std::chrono::duration<double, std::micro>(updateVend - updateVbegin).count();
      if(V_.diagonal_sum()) v_size = 0;
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    parallel_duration += std::chrono::duration<double, std::micro>(t2 - t1);
    //END OF PARALLEL REGION

    /*
    #pragma omp critical
    {
    std::cout << "Size of I: " << I_.size() << " and G: " << G_.diagonal_sum() << '\n';
    std::cout << "       with in I: ";
    for (auto i: I_) std::cout << i << " ";
    std::cout << '\n';
    std::cout << "   and G_: ";
    for (int i = 0; i < n; ++i) {
      if (G_.exist(i)) std::cout << i << " ";
    }
    std::cout << '\n';
  } */

    for (auto i: I_) {
        if (!(G_.exist(i))) continue;
        G_.delete_node(i);
        --g_size;
        colors_[i] = current_color;
    }
    //#pragma omp critical
    //std::cout << "G is now that big: " << G_.diagonal_sum() << '\n';
    //#pragma omp critical
    if (G_.diagonal_sum() == 0) g_size = 0;
    //#pragma omp critical
    //std::cout << "g_size: " << g_size << '\n';
    //empty the MIS for the next round
    I_.clear();
    //std::cout << "With g_size " << g_size << " we came to the end." << '\n';
  }
  t2_ = std::chrono::high_resolution_clock::now();
  success_ = 1;
  duration_ = std::chrono::duration<double, std::micro>(t2_- t1_);
  std::cout << "Time spent in parallel section " << parallel_duration.count() << " microseconds\n";
  nr_colors_used_ = current_color;
}

//Coloring Dense Matrix
template<>
void MIS_par<Graph_dense_matrix>::perform_coloring(){
  //START TIMING HERE
  t1_ = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::micro> parallel_duration;
  // Setup
  // Different RNG
  std::mt19937 generator (1337);
  std::uniform_real_distribution<double> dis(0.0, 1.0);

  int n = G_.get_size();
  int current_color = 0;
  degrees_begin_.resize(n);
  I_.resize(0);
  bool unfinished = true;
  int count_while = 1;

  //START OF COLORING
  //double degreedur = 0;
  //auto degreebegin = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < n; i++) {
    degrees_begin_[i] = G_.degree(i);
  }
  //auto degreeend = std::chrono::high_resolution_clock::now();
  //degreedur += std::chrono::duration<double, std::micro>(degreeend - degreebegin).count();

  //double erasedur = 0;
  //double copySdur = 0;
  //double updateVdur = 0;
  //double MISdur = 0;
  auto t1 = std::chrono::high_resolution_clock::now();
  #pragma omp parallel for firstprivate(I_, V_, degrees_begin_)
  for(int g_size = G_.get_size(); g_size > 0; --g_size){
  //while(G_.diagonal_sum() != 0) {
    //++g_size;
    int my_col;
    #pragma omp critical
    {
    ++current_color;
    my_col = current_color;
    }

    std::vector<size_t> degrees_ = degrees_begin_;
    V_ = G_;

    //While V isnt empty set => Determine a MIS
    //auto misbegin = std::chrono::high_resolution_clock::now();
    while(V_.diagonal_sum() != 0){
      // Clear Temps
      std::vector<size_t> S_;

      // Determine degrees of nodes
      //#pragma omp parallel for
      //auto degreebegin = std::chrono::high_resolution_clock::now();
      for (size_t i = 0; i < n; i++) {
        if (V_(i,i) == 0) continue;
        //degrees_[i] = V_.degree(i);

        // Determine random set of vertices S_ in V_
        double r = dis(generator);
        if (!(V_(i,i))) continue;
        if (degrees_[i] == 0) {
          //#pragma omp critical
            S_.push_back(i);
          continue;
        }
        double temp = 1.0/(2.0 * (degrees_[i]));
        if(r < temp) {
          //#pragma omp critical
            S_.push_back(i);
        }
      }

      //auto erasebegin = std::chrono::high_resolution_clock::now();
      for(auto i = S_.begin(); i != S_.end(); ++i){
        for(auto j = S_.begin(); j != S_.end(); ++j){
          if (i == j) continue;
          if(V_(*i,*j)){
            if(degrees_[*i] < degrees_[*j]){
              i=std::prev(i,1);
              S_.erase(std::next(i,1));
              break;
            }
            else{
              j = std::prev(j,1);
              S_.erase(std::next(j,1));
            }
          }
        }
      }
      //auto eraseend = std::chrono::high_resolution_clock::now();
      //erasedur += std::chrono::duration<double, std::micro>(eraseend - erasebegin).count();

      //Add set S_ to I_
      //#pragma omp parallel for
      //auto copySbegin = std::chrono::high_resolution_clock::now();
      for(int x = 0; x < S_.size(); ++x){
        I_.push_back(S_[x]);
      }
      //auto copySend = std::chrono::high_resolution_clock::now();
      //copySdur += std::chrono::duration<double, std::micro>(copySend - copySbegin).count();

      //Remove from V the set S and all the neighbours of nodes in S.
      //#pragma omp parallel for
      //auto updateVbegin = std::chrono::high_resolution_clock::now();
      for (auto s_vertex : S_) {
        for (size_t j = 0; j < n; j++) {
          if (!(V_(j,j))) continue;
          if(V_(s_vertex, j)){
            //lower degree of connected vertices
            for (size_t v = 0; v < n; v++) {
              if ((V_(j,v)))
                --degrees_[v];
            }
            V_.delete_node(j);
          }
        }
        //lower degree of connected vertices
        for (size_t v = 0; v < n; v++) {
          if ((V_(s_vertex,v)))
            --degrees_[v];
        }
        V_.delete_node(s_vertex);
      }
      //auto updateVend = std::chrono::high_resolution_clock::now();
      //updateVdur += std::chrono::duration<double, std::micro>(updateVend - updateVbegin).count();
    }
    //Found MIS in I
    //auto misend = std::chrono::high_resolution_clock::now();
    //MISdur += std::chrono::duration<double, std::micro>(misend - misbegin).count();

    //coloring Set I in current color
    //#pragma omp parallel for
    for (auto i: I_) {
        if (!(G_(i,i))) continue;
        //lower degree of connected vertices
        for (size_t v = 0; v < n; ++v) {
          if ((G_(i,v))) {
            #pragma omp critical
            --degrees_begin_[v];
          }
        }
        G_.delete_node(i);
        colors_[i] = my_col;
    }
    if (!(G_.diagonal_sum())) g_size = 0;

    //empty the MIS for the next round
    I_.clear();
  }
  auto t2 = std::chrono::high_resolution_clock::now();

  t2_ = std::chrono::high_resolution_clock::now();
  success_ = 1;
  duration_ = std::chrono::duration<double, std::micro>(t2_- t1_);
  nr_colors_used_ = current_color;
  parallel_duration += std::chrono::duration<double, std::micro> (t2 - t1);
  std::cout << "Time spent in parallel section " << parallel_duration.count() << " microseconds\n";
  //std::cout << "Degree Calculation in\t"   << degreedur         << " microseconds!\n";
  //std::cout << "Updating S_ finished in\t" << erasedur          << " microseconds!\n";
  //std::cout << "Copying S_ finished in\t"  << copySdur          << " microseconds!\n";
  //std::cout << "Updating V_ finished in\t" << updateVdur        << " microseconds!\n";
  //std::cout << "MIS search finished in\t " << MISdur            << " microseconds!\n";
  //std::cout << "finished in "              << duration_.count() << " microseconds!\n";
}
