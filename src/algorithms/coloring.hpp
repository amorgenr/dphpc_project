//Abstract base class for all the coloring algorithms
#ifndef COLORING_HPP
#define COLORING_HPP

//graph
#include "graph.hpp"
#include "adlist_graph.hpp"
//#include "dense_matrix_graph.hpp"

//timing
#include <chrono>
#include <ctime>

//other
#include <vector>
#include <algorithm>

//printing
#include <iostream>
#include <fstream>
#include <string>

//openmp
#include <omp.h>

template<class Gr>
class Coloring {
protected:

  //number of vertices
  size_t n_;
  //true if coloring succeeded
  bool success_;
  //time points
  std::chrono::high_resolution_clock::time_point t1_;
  std::chrono::high_resolution_clock::time_point t2_;
  //the graph
  Gr G_;
  //number of colors used
  size_t nr_colors_used_ = 1;
  //duration of coloring
  std::chrono::duration<double, std::micro> duration_;
  //vector representing the colors of the nodes
  std::vector<size_t> colors_;
  //
  bool correct_;

  size_t nr_threads_;

public:
  Coloring (const Gr&);
  //class specialised
  virtual void perform_coloring() = 0;
  void check_correctness(bool output = true);

  inline double get_duration() const {
    return duration_.count();
  }
  inline size_t get_nr_colors() const {
    return nr_colors_used_;
  }
  inline size_t get_nr_threads_() const {

    return nr_threads_;
  }
  inline bool is_correct() const;

  void get_colors() const {
    std::cout << "vertex:" << "    " << "color:" << std::endl;
    for (size_t i = 0; i < colors_.size(); i++) {
      std::cout << "       " << i << "         " << colors_[i] << std::endl;
    }
    std::cout << std::endl;
  }
  void write_to_csv(std::string, bool append = false, bool date = true) const;

};

template<class Gr>
Coloring<Gr>::Coloring(const Gr& G):
  G_(G),
  nr_colors_used_(0),
  success_(false),
  n_(G.get_size()),
  colors_(std::vector<size_t>(n_, 0)),
  correct_{false},
  nr_threads_{1}{
    unsigned int thread_qty = std::max(atoi(std::getenv("OMP_NUM_THREADS")), 1);
    omp_set_num_threads(thread_qty);
    nr_threads_ = thread_qty;
}

template<class Gr>
void Coloring<Gr>::check_correctness(bool output){
  for(size_t i = 0; i<n_; i++){
    if(colors_[i]==0){
      if(output)std::cout << i << " is not colored!\n";
      return;
    }
    for(size_t j = i + 1; j<n_; j++){
      if(G_(i,j)== true && colors_[i] == colors_[j]){
        if(output)std::cout << "coloring not correct!\n" << i <<" and " << j <<"are connected, but color i: " << colors_[i] << "==" << colors_[j]  << std::endl;
        return;
      }
    }
  }
  correct_ = true;
}

template<>
void Coloring<Graph_adlist>::check_correctness(bool output){
  for (size_t i = 0; i < n_; i++) {
    if(colors_[i]==0){
      if(output)std::cout << i << " is not colored!\n";
      return;
    }
    for (auto j: G_.get_list(i)) {
      if(colors_[i]==colors_[j]){
        if(output)std::cout << "coloring not correct!\n" << i <<" and " << j <<"are connected, but color i: " << colors_[i] << "==" << colors_[j]  << std::endl;
        return;
      }
    }
  }
  correct_ = true;
}


template<class Gr>
void Coloring<Gr>::write_to_csv(std::string filename, bool append, bool date) const {
  std::ofstream myfile;

  std::time_t t = std::time(0);
  std::tm* now = std::localtime(&t);
  if(date){
    filename +=    std::to_string((now->tm_mon + 1)) + '-'
                +  std::to_string(now->tm_mday) + '-'
                +  std::to_string(now->tm_hour)
                +  std::to_string(now->tm_min)
                +  std::to_string(now->tm_sec);
  }
  if(append) myfile.open("../data/" + filename + ".csv", std::ios::app);
  else myfile.open("../data/" + filename + ".csv");
  myfile << (!append ? "Correctness,Size,Probability,Time,Colors,Threads\n" : "")
         << correct_ << "," << n_ << "," << G_.get_probability() << "," << get_duration() << "," << get_nr_colors() << "," << get_nr_threads_() << "\n";
  myfile.close();
}

template<class Gr>
bool Coloring<Gr>::is_correct() const {
    return correct_;
}

#endif
