#ifndef JONES_PAR_HPP
#define JONES_PAR_HPP

//coloring
#include "coloring.hpp"

//graph
#include "graph.hpp"
#include "dense_matrix_graph.hpp"
#include "adlist_graph.hpp"

//parallel stuff
#include <omp.h>

//other
#include "algorithm" //for find_if in perform_coloring()

template<class Gr>
class Jones_par: public Coloring<Gr> {
private:
  inline bool has_prio(const size_t);
public:
  Jones_par(const Gr&);
  //virtual ~Jones_par ();
  void perform_coloring();
};

template<class Gr>
  Jones_par<Gr>::Jones_par(const Gr& G): Coloring<Gr>(G){
}

template<class Gr>
void Jones_par<Gr>::perform_coloring(){
  std::cout << "parallel coloring!";
}

template<>
bool Jones_par<Graph_dense_matrix>::has_prio(const size_t i){
  #pragma parallel for
  for (size_t j = i + 1; j < n_; j++) {
    if(G_(i,j) && !colors_[j]) return false;
  }
  return true;
}

template<>
bool Jones_par<Graph_adlist>::has_prio(const size_t i){
  #pragma parallel for
  for (auto j: G_.get_list(i)) {
    //has_prio if is greater than all uncolored neighbors
    //if we change adlist to save only larger neighbors this still works but can remove 1st if statement (about n^2 * p / 2 (= nr. of edges where j<i) comparisons)
    if(j < i) continue;
    if(!colors_[j]) return false;
  }
  return true;
}

template<>
void Jones_par<Graph_dense_matrix>::perform_coloring(){
  //TIMING START
  t1_ = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::micro> parallel_duration1;
  std::chrono::duration<double, std::micro> parallel_duration2;
  //vector as queue
  std::vector<size_t> Q;

  //vector to find available colors
  std::vector<int> colors_avail_;

  //current color number, set to 1 to color the last node directly.
  size_t current_color = 1;
  //color the last node to avoid if statement later
  colors_.back() = 1;

  while(!success_){
    //find relevant nodes



    auto t1 = std::chrono::high_resolution_clock::now();
    #pragma omp parallel
    {
    #pragma omp for
    for (int i = 0; i < n_ - 1; i++) {
      if(colors_[i]) continue;
      if(has_prio(i)) Q.push_back(i);
      //not optimal since it is assigned in for loop
    }
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    parallel_duration1 += std::chrono::duration<double, std::micro> (t2 - t1);



    //return if we are finished
    if(Q.empty()) success_ = true;
    else{
      size_t min_col = 0;
      colors_avail_.resize(current_color+1);
      //fill with true values
      std::fill(colors_avail_.begin(), colors_avail_.end(), true);



      t1 = std::chrono::high_resolution_clock::now();
      //This breaks coloring, add
      #pragma omp parallel
      {
      #pragma omp for
      for (size_t i = 0; i < Q.size(); i++) {
        size_t temp = Q[i];
        for (size_t j = temp + 1; j < n_; j++) {
          //if there exists an edge between i and j make the color of j unavailable
          if(G_(temp,j)) colors_avail_[colors_[j]] = false;
        }
      }
      }
      t2 = std::chrono::high_resolution_clock::now();
      parallel_duration2 += std::chrono::duration<double, std::micro> (t2 - t1);



      //find the minimum color
      //avoid call to .size() every iteration
      int size = colors_avail_.size();

      for (int i = 1; i < size; i++) {
        if (colors_avail_[i]) {
          min_col = i;
          break;
        }
      }

      //if there is no already used color, define a new color
      if(min_col == 0){
        min_col = ++current_color;
      }

      //fill all relevant colors_
      for (int i = 0; i < Q.size(); i++) {
        colors_[Q[i]] = min_col;
      }
      Q.clear();
    }
  }

  t2_ = std::chrono::high_resolution_clock::now();
  duration_ = std::chrono::duration<double, std::micro>(t2_ - t1_);
  nr_colors_used_ = current_color;
  std::cout << "Time spent in parallel section 1: " << parallel_duration1.count() << " microseconds\n";
  std::cout << "Time spent in parallel section 2: " << parallel_duration2.count() << " microseconds\n";
}

template<>
void Jones_par<Graph_adlist>::perform_coloring(){
  t1_ = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::micro> parallel_duration;
  //vector as queue
  std::vector<size_t> Q;

  //vector to find available colors
  std::vector<int> colors_avail_;

  //current color number, set to 1 to color the last node directly.
  size_t current_color = 1;
  //color the last node to avoid if statement later
  colors_.back() = 1;

  while(!success_){
  //find highest prio nodes


  auto t1 = std::chrono::high_resolution_clock::now();
    #pragma omp parallel for
    for (int i = 0; i < n_ - 1; i++) {
      if (colors_[i]) continue;
      if (has_prio(i)) Q.push_back(i);
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    parallel_duration += std::chrono::duration<double, std::micro> (t2 - t1);


    if(Q.empty()){
      success_ = true;
      break;
    }

    //find lowest possible color
    size_t min_col = 0;
    colors_avail_.resize(current_color + 1);
    //fill with true values
    std::fill(colors_avail_.begin(), colors_avail_.end(), true);


    t1 = std::chrono::high_resolution_clock::now();
    //#pragma omp parallel for
    for (int i = 0; i < Q.size(); i++) {
      for (auto j: G_.get_list(Q[i])) {
        colors_avail_[colors_[j]] = false;
      }
    }
    t2 = std::chrono::high_resolution_clock::now();
    parallel_duration += std::chrono::duration<double, std::micro> (t2 - t1);



    //find the minimum color
    for (size_t i = 1; i < colors_avail_.size(); i++) {
      if (colors_avail_[i]) {
        min_col = i;
        break;
      }
    }

    //if there is no already used color, define a new color
    if(min_col == 0){
      min_col = ++current_color;
    }

    //fill all relevant colors_
    for (int i = 0; i < Q.size(); i++) {
      colors_[Q[i]] = min_col;
    }
    Q.clear();
  }

  t2_ = std::chrono::high_resolution_clock::now();
  success_ = true;
  duration_ = std::chrono::duration<double, std::micro>(t2_ - t1_);
  nr_colors_used_ = current_color;
  std::cout << "Time spent in parallel section: " << parallel_duration.count() << " microseconds\n";
}

#endif
