import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas

#seed
np.random.seed(1337)

#max_nr_threads
max_n = 30

#linear speedup
x = np.linspace(0, max_n, 1000)
y = x

p_JP_MAT  = 0.67
p_JP_ADL  = 0.925
p_MIS_MAT = 0.9
p_MIS_ADL = 0.99
ptest = 0.98

x_amdahl  = np.arange(1.0, max_n, 1.0)
y1 = 1.0/(1.0 - p_JP_MAT + p_JP_MAT/x_amdahl)
y2 = 1.0/(1.0 - p_JP_ADL + p_JP_ADL/x_amdahl)

#parameters:
#data parameters
JP_ADL_filename  = '../../data/MIS_mat_par_speed_up.csv'
JP_MAT_filename  = '../../data/JP_mat_par_speed_up.csv'

#plotting choices
totalplot_xlabel = "No. of threads"
totalplot_ylabel = "Runtime [s]"
legend_fontsize = 'medium'

scatterplot_color1 = "r"
scatterplot_alpha1 = 0.7
scatterplot_label1 = "Jones-Plassman (Matrix)"

scatterplot_color2 = "g"
scatterplot_alpha2 = 0.7
scatterplot_label2 = "MIS (Matrix)"

scatterplot_color3 = "r"
scatterplot_alpha3 = 0.7
scatterplot_label3 = "MIS (matrix)"

scatterplot_color4 = "g"
scatterplot_alpha4 = 0.7
scatterplot_label4 = "MIS (adlist)"

#plotting save options
folder_path = '../../data/plots/'
file_name = 'coloring_optimality'
file_type = '.png'
save_path = folder_path + file_name + file_type

#code:
#csv file reading
dfJP_MAT  = pandas.read_csv(JP_MAT_filename)
dfJP_ADL  = pandas.read_csv(JP_ADL_filename)

threads_temp      = dfJP_ADL['Threads'].tolist()
time_JP_MAT_temp  = dfJP_MAT['Time'].tolist()
time_JP_ADL_temp  = dfJP_ADL['Time'].tolist()
colors_JP_MAT_temp = dfJP_MAT['Colors'].tolist()
colors_JP_ADL_temp = dfJP_ADL['Colors'].tolist()

#Average Time Values
threads = []
time_JP_MAT = []
time_JP_ADL = []
colors_JP_ADL = []
colors_JP_MAT = []
for i in range(1,len(time_JP_MAT_temp)//10):
    threads.append(threads_temp[10*i])
    time_JP_MAT.append(np.average(time_JP_MAT_temp[10*i:10*i+10])/1000000)
    time_JP_ADL.append(np.average(time_JP_ADL_temp[10*i:10*i+10])/1000000)
    colors_JP_MAT.append(int(np.average(colors_JP_MAT_temp[10*i:10*i+10])))
    colors_JP_ADL.append(int(np.average(colors_JP_ADL_temp[10*i:10*i+10])))


speedup_JP_MAT =  time_JP_MAT
speedup_JP_ADL =  time_JP_ADL

fig = plt.figure()
ax = fig.add_subplot(111)

plt.title("Coloring Optimality (n=10'000, p=0.015)",loc ='left' ,fontweight='bold', x=-0.04, y=1.06)

xlabel = ax.set_xlabel(totalplot_xlabel)
ylabel = ax.set_ylabel(totalplot_ylabel, rotation=0)

ax.set_ylim([0, 7])
ax.set_axisbelow('True')
ax.spines['left'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.grid(color='w',linewidth=2)
ax.patch.set_facecolor('0.92')
ax.yaxis.set_label_coords(0.043,1.03)

#1st subplot -- Jones-Plassman
# linearscaling_JP = ax.plot(x, y, 'k--', label='Linear Speedup')
# amdahlscaling_JP_MAT = ax.plot(x_amdahl, y1, 'k-.', c = scatterplot_color1, label='possible speedup - Matrix')
# amdahlscaling_JP_ADL = ax.plot(x_amdahl, y2, 'k-.', c = scatterplot_color2,label='possible speedup - Adlist')
JP_MAT_scatter = ax.scatter(threads, speedup_JP_MAT, marker = '^', c = scatterplot_color1, s=10, alpha = scatterplot_alpha1, label=scatterplot_label1)
JP_ADL_scatter = ax.scatter(threads, speedup_JP_ADL, marker = 'v', c = scatterplot_color2, s=10, alpha = scatterplot_alpha2, label=scatterplot_label2)

for i in range(len(colors_JP_ADL)):
    if(i%2 == 0):
        ax.annotate(colors_JP_MAT[i], (threads[i], speedup_JP_MAT[i]),textcoords = "offset points",xytext = (0, 5),ha='center',fontsize='small')
    else:
        ax.annotate(colors_JP_MAT[i], (threads[i], speedup_JP_MAT[i]),textcoords = "offset points",xytext = (0, -10 ),ha='center',fontsize='small')
    ax.annotate(colors_JP_ADL[i], (threads[i], speedup_JP_ADL[i]),textcoords = "offset points",xytext = (0, 5),ha='center',fontsize='small')

#google: hex color
legend1 = ax.legend(loc = 'upper right', shadow=True, fontsize=legend_fontsize)

legend1.get_frame().set_facecolor('#dedede')

plt.show()
fig.savefig(save_path, dpi=800)
