import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas

#seed
np.random.seed(1337)

#code:
#csv file reading
JP_MAT_filename  = '../../data/JP_mat_seqvaryingp.csv'
JP_ADL_filename  = '../../data/JP_adl_seqvaryingp.csv'
MIS_MAT_filename = '../../data/MIS_mat_seq.csv'
MIS_ADL_filename = '../../data/MIS_adl_seq.csv'

dfJP_MAT  = pandas.read_csv(JP_MAT_filename)
dfJP_ADL  = pandas.read_csv(JP_ADL_filename)
# dfMIS_MAT = pandas.read_csv(MIS_MAT_filename)
# dfMIS_ADL = pandas.read_csv(MIS_ADL_filename)

size_temp         = dfJP_MAT['Probability'].tolist()
time_JP_MAT_temp  = dfJP_MAT['Time'].tolist()
time_JP_ADL_temp  = dfJP_ADL['Time'].tolist()
# time_MIS_MAT_temp = dfMIS_MAT['Time'].tolist()
# time_MIS_ADL_temp = dfMIS_ADL['Time'].tolist()

#Average Time Values
size = []
time_JP_MAT = []
time_JP_ADL = []
time_MIS_MAT = []
time_MIS_ADL = []
for i in range(len(time_JP_MAT_temp)//10):
    size.append(size_temp[10*i])
    time_JP_MAT.append(np.average(time_JP_MAT_temp[10*i:10*i+10]))
    time_JP_ADL.append(np.average(time_JP_ADL_temp[10*i:10*i+10]))
    # time_MIS_MAT.append(int(np.average(time_MIS_MAT_temp[10*i:10*i+10])))
    # time_MIS_ADL.append(int(np.average(time_MIS_ADL_temp[10*i:10*i+10])))

#plotting choices
totalplot_xlabel = "Probability p"
totalplot_ylabel = "Runtime [$\mu s$]"
legend_fontsize = 'small'

scatterplot_color1 = "r"
scatterplot_alpha1 = 0.7
scatterplot_label1 = "Jones-Plassmann (Matrix)"

scatterplot_color2 = "g"
scatterplot_alpha2 = 0.7
scatterplot_label2 = "Jones-Plassmann (Adlist)"

scatterplot_color3 = "b"
scatterplot_alpha3 = 0.7
scatterplot_label3 = "MIS (matrix)"

scatterplot_color4 = "y"
scatterplot_alpha4 = 0.7
scatterplot_label4 = "MIS (Adlist)"

#plotting save options
folder_path = '../../data/plots/'
file_name = 'varying_p'
file_type = '.png'
save_path = folder_path + file_name + file_type

#plotting
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim([10**2, 10**5])
plt.title('Edge Probability - Runtime , n = 1000',loc ='left' ,fontweight='bold', x=-0.065, y=1.06)
ax.set_yscale('log')
ax.set_xscale('log')
xlabel = ax.set_xlabel(totalplot_xlabel)
ylabel = ax.set_ylabel(totalplot_ylabel, rotation=0)

ax.set_axisbelow('True')
ax.spines['left'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.grid(color='w',linewidth=2)
ax.patch.set_facecolor('0.92')
ax.yaxis.set_label_coords(0.025,1.03)


#1st subplot -- Jones-Plassman
JP_MAT_scatter = ax.scatter(size, time_JP_MAT, marker = '^', c = scatterplot_color1, alpha = scatterplot_alpha1, label=scatterplot_label1)
JP_ADL_scatter = ax.scatter(size, time_JP_ADL, marker = 'v', c = scatterplot_color2, alpha = scatterplot_alpha2, label=scatterplot_label2)

# #2nd subplot -- MIS
# MIS_MAT_scatter = ax.scatter(size, time_MIS_MAT, c = scatterplot_color3, alpha = scatterplot_alpha3, label=scatterplot_label3)
# MIS_ADL_scatter = ax.scatter(size, time_MIS_ADL, c = scatterplot_color4, alpha = scatterplot_alpha4, label=scatterplot_label4)

legend = ax.legend(loc = 'upper left', shadow=True, fontsize=legend_fontsize)
legend.get_frame().set_facecolor('#dedede')

plt.show()
fig.savefig(save_path, dpi=800)
