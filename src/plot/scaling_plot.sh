#!/bin/bash
# call this script in euler from /build using
# ./../src/plotting/scaling_plot.sh
# choose a fixed n (50'000), p (minimal) and repetitions (10) in euler_base
# only works if OMP_NUM_THREADS is not set in scripts!!!!!
if [[ ${PWD##*/} =  "build" ]]; then
  mkdir -p ../data/plots
  module load new
  module load cmake/3.11.4  eigen/3.2.1 boost/1.62.0 gcc/6.3.0
  module load new
  module load cmake/3.11.4  eigen/3.2.1 boost/1.62.0 gcc/6.3.0

  make -j4

  echo "Delete all old csv files in data?"
  echo "y -- yes | n -- no"
  read delete
  if [[ $delete = "y" ]]; then
    echo "y -> deleting all csv-files in /data"
    rm ../data/*.csv
  elif [[ $delete = "n" ]]; then
    echo "n -> no files were deleted."
    echo "Be careful about overwriting/not writing/other possible errors."
  else
    echo "Input neither y nor n: No files were deleted."
    echo "Be careful about overwriting/not writing/other possible errors."
  fi

  echo "Min nr. of threads?"
  read min_thread
  echo "Max nr. of threads?"
  read max_thread
  echo "step size?"
  read step_size
  
  helper=`expr $min_thread + 1`
  echo "hello$helper"
  bsub -n $min_thread -J "job1" "export OMP_NUM_THREADS=$min_thread; ./src/euler/euler_base"
  for (( c= $helper; c<=$max_thread; c += $step_size ))
    do
    d=`expr $c - $min_thread`
    e=`expr $d + 1`
    bsub -n $c -J "job$e" -w "done(job$d)" "export OMP_NUM_THREADS=$c; ./src/euler/euler_base"
  done

  echo "to kill all: bkill 0"
  echo "------------------------"
  echo "data is written to data/"
else
  echo "go to build directory and use the command ../src/plotting/scaling_plot.sh"
fi
