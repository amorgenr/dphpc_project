import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas
#seed
np.random.seed(1337)


#max_nr_threads
max_n = 32

#linear speedup
x = np.linspace(0, max_n, 1000)
y = x


p_JP_MAT  = 0.67
p_JP_ADL  = 0.925
p_MIS_MAT = 0.9
p_MIS_ADL = 0.99
ptest = 0.98

x_amdahl  = np.arange(1.0, max_n, 1.0)
y1 = 1.0/(1.0 - p_JP_MAT + p_JP_MAT/x_amdahl)
y2 = 1.0/(1.0 - p_JP_ADL + p_JP_ADL/x_amdahl)
y3 = 1.0/(1.0 - p_MIS_MAT + p_MIS_MAT/x_amdahl)
y4 = 1.0/(1.0 - p_MIS_ADL + p_MIS_ADL/x_amdahl)

#parameters:
#data parameters
JP_MAT_filename  = '../../data/JP_mat_par_speed_up.csv'
JP_ADL_filename  = '../../data/JP_adl_par_speed_up.csv'
MIS_MAT_filename = '../../data/MIS_mat_par_speed_up.csv'
MIS_ADL_filename = '../../data/MIS_adl_par_speed_up.csv'

#plotting choices
totalplot_xlabel = "No. of Threads"
totalplot_ylabel = "[Speedup/1]"
legend_fontsize = 'small'

scatterplot_color1 = "r"
scatterplot_alpha1 = 0.7
scatterplot_label1 = "Jones-Plassman (Matrix)"

scatterplot_color2 = "g"
scatterplot_alpha2 = 0.7
scatterplot_label2 = "Jones-Plassman (Adlist)"

scatterplot_color3 = "r"
scatterplot_alpha3 = 0.7
scatterplot_label3 = "MIS (Matrix)"

scatterplot_color4 = "g"
scatterplot_alpha4 = 0.7
scatterplot_label4 = "MIS (Adlist)"

#plotting save options
folder_path = '../../data/plots/'
file_name = 'speed-up'
file_type = '.png'
save_path = folder_path + file_name + file_type

#code:
#csv file reading
dfJP_MAT  = pandas.read_csv(JP_MAT_filename)
dfJP_ADL  = pandas.read_csv(JP_ADL_filename)
dfMIS_MAT = pandas.read_csv(MIS_MAT_filename)
dfMIS_ADL = pandas.read_csv(MIS_ADL_filename)

threads_temp      = dfJP_MAT['Threads'].tolist()
time_JP_MAT_temp  = dfJP_MAT['Time'].tolist()
time_JP_ADL_temp  = dfJP_ADL['Time'].tolist()
time_MIS_MAT_temp = dfMIS_MAT['Time'].tolist()
time_MIS_ADL_temp = dfMIS_ADL['Time'].tolist()
colors_JP_ADL_temp = dfJP_ADL['Colors'].tolist()
colors_MIS_ADL_temp = dfMIS_ADL['Colors'].tolist()

#  IF NEEDED AVERAGE MULTIPLE VALUES
#Average Time Values
threads = []
time_JP_MAT = []
time_JP_ADL = []
time_MIS_MAT = []
time_MIS_ADL = []
colors_JP_ADL = []
colors_MIS_ADL = []
for i in range(len(time_JP_MAT_temp)//10):
    threads.append(threads_temp[10*i])
    time_JP_MAT.append(np.average(time_JP_MAT_temp[10*i:10*i+10]))
    time_JP_ADL.append(np.average(time_JP_ADL_temp[10*i:10*i+10]))
    time_MIS_MAT.append(np.average(time_MIS_MAT_temp[10*i:10*i+10]))
    time_MIS_ADL.append(np.average(time_MIS_ADL_temp[10*i:10*i+10]))
    colors_JP_ADL.append(int(np.average(colors_JP_ADL_temp[10*i:10*i+10])))
    # colors_MIS_ADL.append(int(np.average(colors_MIS_ADL_temp[10*i:10*i+10])))

speedup_JP_MAT = time_JP_MAT[0] / time_JP_MAT
speedup_JP_ADL = time_JP_ADL[0] / time_JP_ADL
speedup_MIS_MAT = time_MIS_MAT[0] / time_MIS_MAT
speedup_MIS_ADL = time_MIS_ADL[0] / time_MIS_ADL

fig = plt.figure()
ax = fig.add_subplot(111)
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
ax1.set_ylim([0, 32])
ax2.set_ylim([0, 32])
ax1.set_title('Jones-Plassmann Strong Scaling', loc='left', fontweight='bold', x=-0.08, y=1.03)
ax2.set_title('MIS Strong Scaling', loc='left', fontweight='bold', x=-0.085, y=1.03)

ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='w', top='False', bottom='False', left='False', right='False')
ax1.set_xlabel(totalplot_xlabel, rotation=0)
ax2.set_xlabel(totalplot_xlabel, rotation=0)
ax1.set_ylabel(totalplot_ylabel, rotation=0)
ax2.set_ylabel(totalplot_ylabel, rotation=0)

ax1.set_axisbelow('True')
ax1.spines['left'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.yaxis.grid(color='w',linewidth=2)
ax1.patch.set_facecolor('0.92')
ax1.yaxis.set_label_coords(0.039,1)

ax2.set_axisbelow('True')
ax2.spines['left'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.yaxis.grid(color='w',linewidth=2)
ax2.patch.set_facecolor('0.92')
ax2.yaxis.set_label_coords(0.039,1)

#1st subplot -- Jones-Plassman
linearscaling_JP = ax1.plot(x, y, 'k--', label='Linear Speedup')
amdahlscaling_JP_MAT = ax1.plot(x_amdahl, y1, 'k:', c = scatterplot_color1, label='Possible Speedup - Matrix')
amdahlscaling_JP_ADL = ax1.plot(x_amdahl, y2, 'k-.', c = scatterplot_color2, label='Possible Speedup - Adlist')
JP_MAT_scatter = ax1.scatter(threads, speedup_JP_MAT, marker = '^', c = scatterplot_color1, s=10, alpha = scatterplot_alpha1, label=scatterplot_label1)
JP_ADL_scatter = ax1.scatter(threads, speedup_JP_ADL, marker = 'v',c = scatterplot_color2, s=10, alpha = scatterplot_alpha2, label=scatterplot_label2)
for i in range(len(colors_JP_ADL)):
    ax.annotate(colors_JP_ADL[i], (threads[i], speedup_JP_ADL[i]),textcoords = "offset points",xytext = (0,-15),ha='center',fontsize='xx-small')

#2nd subplot -- MIS
linearscaling_MIS = ax2.plot(x, y, 'k--', label='Linear Speedup')
amdahlscaling_MIS_MAT = ax2.plot(x_amdahl, y3, 'k:', c = scatterplot_color3, label='Possible Speedup - Matrix')
amdahlscaling_MIS_MAT = ax2.plot(x_amdahl, y4, 'k-.', c = scatterplot_color4, label='Possible Speedup - Adlist')
MIS_MAT_scatter = ax2.scatter(threads, speedup_MIS_MAT, marker = '^', c = scatterplot_color3, s=10, alpha = scatterplot_alpha3, label=scatterplot_label3)
MIS_ADL_scatter = ax2.scatter(threads, speedup_MIS_ADL, marker = 'v', c = scatterplot_color4, s=10, alpha = scatterplot_alpha4, label=scatterplot_label4)
for i in range(len(colors_MIS_ADL)):
    ax.annotate(colors_MIS_ADL[i], (threads[i], speedup_MIS_MAT[i]),textcoords = "offset points",xytext = (0,-15),ha='center',fontsize='xx-small')

#google: hex color
legend1 = ax1.legend(loc = 'upper left', shadow=True, fontsize=legend_fontsize)
legend2 = ax2.legend(loc = 'upper left', shadow=True, fontsize=legend_fontsize)

legend1.get_frame().set_facecolor('#dedede')
legend2.get_frame().set_facecolor('#dedede')

plt.show()
fig.set_size_inches(10, 5)
fig.savefig(save_path, dpi=800)
