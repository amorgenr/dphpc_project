import matplotlib.pyplot as plt
import numpy as np
import pandas
#remove the following in code!
#necessary for .__version__
import matplotlib
#necessary for check if folder exists
import os

#seed
np.random.seed(1337)

#TEMP
#data - usually import from ../../data/datafile.csv
x = np.arange(0.0, 10.0, 1.0)
y = np.cos(np.pi * x / 10.) + np.random.rand()
#\TEMP

#parameters:
#data parameters
'''
csv_filename = '../../data/test.csv'
'''

#plotting choices
scatterplot_color = "r"
scatterplot_alpha = 0.7
scatterplot_label = "dataset"
scatterplot_xlabel = "X-Label"
scatterplot_ylabel = "Y-Label"

#plotting save options
folder_path = '../../data/plots/'
file_name = 'test'
file_type = '.eps'
save_path = folder_path + file_name + file_type

#code:
#csv file reading
#Here it is very important that column names are accessible, so call write_to_csv once without append
'''
df = pandas.read_csv(csv_filename)
#example
xdata = df['size']
ydata = df['time_avg']
'''

#plotting change to xdata and ydata from csv
plt.scatter(x,y,c = scatterplot_color, alpha = scatterplot_alpha, label = scatterplot_label)
plt.xlabel(scatterplot_xlabel)
plt.ylabel(scatterplot_ylabel)
plt.legend(loc = 'upper left')
#optional
'''
plt.show()
'''

#checks
if os.path.isdir(folder_path):
    #DOESN'T WORK ON SLAB, NEEDS NEWER MATPLOTLIB
    #save to .eps for report
    if matplotlib.__version__ >= "3.1.1":
        fig.savefig(save_path, dpi = 1000)
    else:
        print("\n  ERROR:\n  If you want to create high-DPI plots: \n  upgrade matplotlib to a version >= 3.1.1")
else:
    print(f"\n  ERROR:\n  The save path folder does not exist: \n  please create the folder: {folder_path}")
