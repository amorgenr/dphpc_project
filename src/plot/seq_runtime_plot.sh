#!/bin/bash
# call this script in euler from /build using
# ./../src/plotting/seq_runtime_plot.sh
if [[ ${PWD##*/} =  "build" ]]; then
  mkdir -p ../data/plots
  module load new
  module load cmake/3.11.4  eigen/3.2.1 boost/1.62.0 gcc/6.3.0
  module load new
  module load cmake/3.11.4  eigen/3.2.1 boost/1.62.0 gcc/6.3.0
  make -j4

  echo "Delete all old csv files in data?"
  echo "y -- yes | n -- no"
  read delete
  if [[ $delete = "y" ]]; then
    echo "y -> deleting all csv-files in /data"
    rm ../data/*.csv
  elif [[ $delete = "n" ]]; then
    echo "n -> no files were deleted."
    echo "Be careful about overwriting/not writing/other possible errors."
  else
    echo "Input neither y nor n: No files were deleted."
    echo "Be careful about overwriting/not writing/other possible errors."
  fi
  echo "Running sequential runtime comparison"
  bsub -n 1 -J "sequential runtime" 'export OMP_NUM_THREADS=1; ./src/euler/euler_base'
  echo "to kill all: bkill 0"
  echo "------------------------"
  echo "data is written to data/"
else
  echo "go to build directory and use the command ../src/plotting/scaling_plot.sh"
fi

