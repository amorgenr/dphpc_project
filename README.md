# DPHPC_Project
**[Course Website](http://spcl.inf.ethz.ch/Teaching/2019-dphpc/)**


**Project Description:**\
We are analysing the performance of 2 graph coloring algorithms on graphs of varying size and density.\
We are specifically interested in the performance benefits (if any exist) that one can achieve by parallelizing the coloring algorithms using OpenMP. \
The algorithms we are investigating are:\
    -**Jones-Plassmann**\
    -**Luby's**
<!--
    Commented out for now
    -Greedy Multi-Coloring Algorithm (sequential)\)
    -Parallel Greedy? -->

**Dependencies:**

| Library:   | Command(Linux):                       |
| -----      | :--------                             |
| make/cmake | ` sudo apt install cmake `            |
| eigen      | ` sudo apt install libeigen3-dev `    |

<!--  | gtest      | ` sudo apt-get install libgtest-dev ` | -->
<!--  | boost      | ` sudo apt install libboost-all-dev ` | -->

**Naming Conventions:** \
    -Class: Upper_then_lower_case\
    -Member Function: all_lower_case()\
    -(Private) Class Variable: var_\
    -Test File: test_algorithm_testcase.cpp\
    -all graph files end with graph and are based on graph.hpp. provide a .hpp. \
    -all coloring algorithm files are based on coloring.hpp.
    -provide documentation!.

**How to Test:**\
    -In src/tests/test_testname.cpp\
`GraphType GraphName();`\
`Coloring<GraphType> ColoringName(GraphName);`\
`ColoringName.perform_coloring();`\
`ColoringName.write_to_csv(filename, optional append=false, optional date=true)`\
    -In top level run:\
`mkdir data`\
    -In build folder run:\
`cmake .. && make && ./src/tests/test_testname`\
    -results should be in data folder saved in Format month-day-hourminutesecond.csv.\
    -If you want the filename to be different than the aforementioned format, give a filename (without .csv) to write_to_csv.

**Timing functions using chrono**\
`std::chrono::duration<double, std::micro> duration;`\
`std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();`\
    -function call / for loop\
`std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();`\
`duration = std::chrono::duration<double, std::micro>(t2 - t1);`\
`std::cout << duration.count() << std::endl;`


**How to Euler:**\
    Read the [manual](https://scicomp.ethz.ch/wiki/Getting_started_with_clusters#SSH_keys) \
    -ssh into euler `ssh nethz@euler.ethz.ch` and login \
    -[create an ssh key](https://gitlab.ethz.ch/help/ssh/README#generating-a-new-ssh-key-pair), and copy the public key into your gitlab.ethz.ch account \
    -pull the current git repository \
    -run `source euler_setup.sh`. This loads the modules we need. \
    -run .sh scripts from build i.e. `../src/plotting/scaling_plot.sh`\
    -to view status: `bbjobs`\
    -to kill all jobs: `bkill 0` (zero)

**OPENMP:**\
    [OpenMP Documentation](https://www.openmp.org/wp-content/uploads/OpenMP4.0.0.pdf)

**BUILD:**\
    -create a build directory on the top level (not inside src!), then proceed as usual. The binaries can then be found at /build/src.\
    -use make -j4 to use multiple threads while compiling

**Things you have to do if you add a new file:**\
    -if necessary, add dependencies in the cmake file on the same level (e.g. if a file in graph/ needs Eigen, add Eigen in graph/CMakeLists.txt and add dependencies)\
    -add the new file to the cmake file on the same level\
    -for executables, add_executable on same level cmake and link everything (just copypaste another target)

  **Literature:**\
    -[Allwright, J. R., et al. "A comparison of parallel graph coloring algorithms." SCCS-666 (1995): 1-19.](https://pdfs.semanticscholar.org/203a/7b17267a28a06808bfb3b0b9571e32d15503.pdf) \
    -[Normann, Per. "Parallel graph coloring: Parallel graph coloring on multi-core CPUs." (2014).](https://www.diva-portal.org/smash/get/diva2:730761/FULLTEXT01.pdf)\
    -[Jones, Mark T., and Paul E. Plassmann. "A parallel graph coloring heuristic." SIAM Journal on Scientific Computing 14.3 (1993): 654-669.](http://ftp.mcs.anl.gov/pub/tech_reports/reports/P246.pdf)

-[Li, Pingfan, et al. "High performance parallel graph coloring on gpgpus." IEEE, 2016.](https://chenxuhao.github.io/docs/ipdpsw-2016.pdf)[ -- GitHub](https://github.com/faportillo/GraphColoring-Cuda)\
    -[Gebremedhin, Assefaw Hadish. "Parallel graph coloring." UNIVERSITY M Thesis University of Bergen Norway Spring (1999).](http://www.ii.uib.no/~assefaw/pub/coloring/thesis.pdf)

**Our Paper:**\
    -(WIP) https://gitlab.ethz.ch/-/ide/project/lknirsch/DPHPC_Report
