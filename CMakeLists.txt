cmake_minimum_required(VERSION 3.10)

set (CMAKE_CXX_STANDARD 17)

project(dphpc_project CXX)

add_subdirectory(src)
